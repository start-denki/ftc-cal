<?php
/* =====================================================================
Project Name : スタート電気・予想燃料調整単価取得
ProgramID    : FuelPrice.php
Copyright    : アスカクリエイト
Notes        :
Modify       :
2017/11/07 越：新規作成
20xx/xx/xx xx:
======================================================================*/
// Ajax通信ではなく、直接URLを叩かれた場合はエラーメッセージを表示
if (
    !(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')
    && (!empty($_SERVER['SCRIPT_FILENAME']) && 'database.php' === basename($_SERVER['SCRIPT_FILENAME']))
    )
{
    die ('このページは直接ロードしないでください。');
}

// 接続文字列 (PHP5.3.6から文字コードが指定できるようになりました)
$dsn = 'mysql:dbname=ftc-energy_cal;host=mysql57.ftc-energy.sakura.ne.jp;charset=utf8';

// ユーザ名
$user = 'ftc-energy';

// パスワード
$password = 'DebLjHZn-';

//ポストで入力値を受け取る
$AreaCd    = $_POST['AreaCd'];         //地区コード
$PriceDate = $_POST['PriceDate'];      //年月    YYYYMMDD

try
{
    // nullで初期化
    $results = null;

    // DBに接続
    $dbh = new PDO($dsn, $user, $password);

    //テーブルのデータを取得する
    $sth = $dbh->prepare("select UnitPrice AS FuelPrice,
                      FORMAT(UnitPrice, 2) AS FuelPriceStr
                      from power_unitprice
                     where AreaCd = :AreaCd
                       And PriceDate = :PriceDate");

    $sth->bindParam(':AreaCd',    $AreaCd,    PDO::PARAM_INT);
    $sth->bindParam(':PriceDate', $PriceDate, PDO::PARAM_STR);

    $sth->execute();
    $results = $sth->fetchAll(PDO::FETCH_ASSOC);

    // JSON形式で出力する
    header('Content-Type: application/json');
    echo json_encode( $results );
    exit;
}
catch (PDOException $e)
{
    // 例外処理
    die('Error:' . $e->getMessage());
}

?>