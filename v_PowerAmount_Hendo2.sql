CREATE View v_PowerAmount_Hendo2(
AreaCd, AreaName, PriceDate, Capacity,
UsedPower, Price,
AjustPrice, Reused, 
StartPrice,
Price2, StartPrice2,
Kifu_Month,
Power_Year,
FTC_Year,
Kifu_Year
)
AS
SELECT A.AreaCd, A.AreaName, B.PriceDate, A.Capacity,
A.UsedPower, A.Price,
(A.UsedPower * B.UnitPrice) AS AjustPrice,
(A.UsedPower * B.ReEnePrice) AS Reused, 
A.StartPrice,
(A.Price + ( (B.UnitPrice * A.UsedPower) + (B.ReEnePrice * A.UsedPower) ) ) AS Price2,
(A.StartPrice + (B.ReEnePrice * A.UsedPower)) AS StartPrice2,
A.Kifu_Month,
A.Power_Year,
A.FTC_Year,
A.Kifu_Year
FROM power_amount_hendo AS A
INNER JOIN power_unitprice AS B ON B.AreaCd = A.AreaCd and B.PriceDate = A.PriceDate
