<?php
/* =====================================================================
Project Name : スタート電気計算処理（ＢＩＧ用）
ProgramID    : database.php
Copyright    : アスカクリエイト
Notes        :
Modify       :
2017/11/18 越：ＢＩＧ用新規作成
2017/12/02 越：予想の燃料調整単価の計算追加
2018/07/21 越：２０１８年７月以降、計算方式が変わるのでテーブルを新規追加 power_big_amountnext
2019/02/02 越：2019年1月以降、計算方式が変わるのでテーブルを新規追加 power_big_amount_hendo
2020/05/13 越：PowerAmount_Hendoに４項目を追加
20xx/xx/xx xx:
======================================================================*/
// Ajax通信ではなく、直接URLを叩かれた場合はエラーメッセージを表示
if (
    !(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')
    && (!empty($_SERVER['SCRIPT_FILENAME']) && 'database_big.php' === basename($_SERVER['SCRIPT_FILENAME']))
    )
{
    die ('このページは直接ロードしないでください。');
}

// 接続文字列 (PHP5.3.6から文字コードが指定できるようになりました)
$dsn = 'mysql:dbname=ftc-energy_cal;host=mysql57.ftc-energy.sakura.ne.jp;charset=utf8';

// ユーザ名
$user = 'ftc-energy';

// パスワード
$password = 'DebLjHZn-';

//ポストで入力値を受け取る
$AreaCd    = $_POST['AreaCd'];         //地区コード
$Capacity  = $_POST['Capacity'];       //容量(KVA)
$PriceDate = $_POST['PriceDate'];      //年月    YYYYMMDD
$PriceQty  = $_POST['PriceQty'];       //１か月電気料金
$UsedPower = $_POST['UsedPower'];      //ご使用量(kWh)
$CalType   = $_POST['CalType'];        //計算式　０：１か月の電気料金　１：ご使用量（kWh)

try
{
    // nullで初期化
    $results = null;

    // DBに接続
    $dbh = new PDO($dsn, $user, $password);

    //テーブルを取得する  2018-07-21関西電力が金額変更
    $sql = '';
    $table = '';

    if (intval($PriceDate) >= 20190101) {
        $table = 'power_big_amount_hendo';
    }
    else {
        if (intval($AreaCd) == 6 and intval($PriceDate) >= 20180701) {
            $table = 'power_big_amountnext';
        }
        else {
            $table = 'power_big_amount';
        }
    }

    //テーブルのデータを取得する　　ＣＡＬＴＹＰＥ　１：ご使用量　０：電気料金
    if ($CalType == '1') {
        if (intval($PriceDate) >= 20190101) {
        $sql = "select E.UsedPower, E.Price,
                      FORMAT(E.Price,0) AS PriceStr,
                      FORMAT( (E.Price * 12),0) AS PriceYear,

                      E.StartPrice,
                      FORMAT(E.StartPrice,0) AS StartPriceStr,
                      FORMAT( (E.StartPrice * 12),0) AS StartPriceYear,

                      FORMAT( (E.Price - E.StartPrice),0) AS DiffPrice,
                      FORMAT( ((E.Price - E.StartPrice) * 12),0) AS DiffPriceYear,
                      FORMAT( ((1 - (E.StartPrice / E.Price)) * 100), 2) AS DisCount,

                      FORMAT( E.Price2 ,0) AS Price2,
                      FORMAT( E.StartPrice2, 0) AS StartPrice2,

                      FORMAT( (E.Price2 * 12),0) AS Price2Year,
                      FORMAT( (E.StartPrice2 * 12), 0) AS StartPrice2Year,

                      FORMAT( (E.Price2 - E.StartPrice2),0) AS DiffPrice2,
                      FORMAT( ((E.Price2 - E.StartPrice2) * 12),0) AS DiffPriceYear2,
                      FORMAT( ((1 - (E.StartPrice2 / E.Price2)) * 100), 2) AS DisCount2,

                      FORMAT( E.Kifu_Month ,0) AS Kifu_Month,
                      FORMAT( E.Power_Year ,0) AS Power_Year,
                      FORMAT( E.FTC_Year   ,0) AS FTC_Year,
                      FORMAT( E.Kifu_Year  ,0) AS Kifu_Year,

                      from
(
SELECT C.UsedPower, C.Price, C.StartPrice, C.UnitPrice, C.ReEnePrice,
C.Price + ( (C.UnitPrice * C.UsedPower) + (C.ReEnePrice * C.UsedPower) ) AS Price2,
C.StartPrice + (C.ReEnePrice * C.UsedPower) AS StartPrice2

from
(SELECT A.UsedPower,
((:Capacity * D.UnitPrice) + A.Price) AS Price,
TRUNCATE ( ((:Capacity * D.StartUnitPrice) + A.StartPrice),0) AS StartPrice,
B.UnitPrice AS UnitPrice, B.ReEnePrice AS ReEnePrice,
A.Kifu_Month, A.Power_Year, A.FTC_Year, A.Kifu_Year
  from " . $table . " AS A
 INNER JOIN power_unitprice AS B ON B.AreaCd = A.AreaCd and B.PriceDate = A.PriceDate
 INNER JOIN power_big_unitprice AS D ON D.AreaCd = A.AreaCd
 where A.AreaCd = :AreaCd
   And A.PriceDate = :PriceDate
   And B.PriceDate = :PriceDate
   And A.UsedPower <= :UsedPower Order By A.UsedPower Desc Limit 1) AS C)
AS E";
        }
        else {
        $sql = "select E.UsedPower, E.Price,
                      FORMAT(E.Price,0) AS PriceStr,
                      FORMAT( (E.Price * 12),0) AS PriceYear,

                      E.StartPrice,
                      FORMAT(E.StartPrice,0) AS StartPriceStr,
                      FORMAT( (E.StartPrice * 12),0) AS StartPriceYear,

                      FORMAT( (E.Price - E.StartPrice),0) AS DiffPrice,
                      FORMAT( ((E.Price - E.StartPrice) * 12),0) AS DiffPriceYear,
                      FORMAT( ((1 - (E.StartPrice / E.Price)) * 100), 2) AS DisCount,

                      FORMAT( E.Price2 ,0) AS Price2,
                      FORMAT( E.StartPrice2, 0) AS StartPrice2,

                      FORMAT( (E.Price2 * 12),0) AS Price2Year,
                      FORMAT( (E.StartPrice2 * 12), 0) AS StartPrice2Year,

                      FORMAT( (E.Price2 - E.StartPrice2),0) AS DiffPrice2,
                      FORMAT( ((E.Price2 - E.StartPrice2) * 12),0) AS DiffPriceYear2,
                      FORMAT( ((1 - (E.StartPrice2 / E.Price2)) * 100), 2) AS DisCount2,

                      0 AS Kifu_Year,
                      0 AS Power_Year,
                      0 AS FTC_Year,
                      0 AS Kifu_Year

                      from
(
SELECT C.UsedPower, C.Price, C.StartPrice, C.UnitPrice, C.ReEnePrice,
C.Price + ( (C.UnitPrice * C.UsedPower) + (C.ReEnePrice * C.UsedPower) ) AS Price2,
C.StartPrice + (C.ReEnePrice * C.UsedPower) AS StartPrice2

from
(SELECT A.UsedPower,
((:Capacity * D.UnitPrice) + A.Price) AS Price,
TRUNCATE ( ((:Capacity * D.StartUnitPrice) + A.StartPrice),0) AS StartPrice,
B.UnitPrice AS UnitPrice, B.ReEnePrice AS ReEnePrice
  from " . $table . " AS A
 INNER JOIN power_unitprice AS B ON B.AreaCd = A.AreaCd
 INNER JOIN power_big_unitprice AS D ON D.AreaCd = A.AreaCd
 where A.AreaCd = :AreaCd
   And B.PriceDate = :PriceDate
   And A.UsedPower <= :UsedPower Order By A.UsedPower Desc Limit 1) AS C)
AS E";
        }

        $sth = $dbh->prepare($sql) ;

        $sth->bindParam(':AreaCd',    $AreaCd,    PDO::PARAM_INT);
        $sth->bindParam(':PriceDate', $PriceDate, PDO::PARAM_STR);
        $sth->bindParam(':Capacity',  $Capacity,  PDO::PARAM_INT);
        $sth->bindParam(':UsedPower', $UsedPower, PDO::PARAM_INT);
    }
    else {
        if (intval($PriceDate) >= 20190101) {
        $sql = "select E.UsedPower, E.Price,
                      FORMAT(E.Price,0) AS PriceStr,
                      FORMAT( (E.Price * 12),0) AS PriceYear,

                      E.StartPrice,
                      FORMAT(E.StartPrice,0) AS StartPriceStr,
                      FORMAT( (E.StartPrice * 12),0) AS StartPriceYear,

                      FORMAT( (E.Price - E.StartPrice),0) AS DiffPrice,
                      FORMAT( ((E.Price - E.StartPrice) * 12),0) AS DiffPriceYear,
                      FORMAT( ((1 - (E.StartPrice / E.Price)) * 100), 2) AS DisCount,

                      FORMAT( E.Price2,0) AS Price2,
                      FORMAT( E.StartPrice, 0) AS StartPrice2,

                      FORMAT( (E.Price2 * 12),0) AS Price2Year,
                      FORMAT( (E.StartPrice2 * 12), 0) AS StartPrice2Year,

                      FORMAT( (E.Price2 - E.StartPrice2),0) AS DiffPrice2,
                      FORMAT( ((E.Price2 - E.StartPrice2) * 12),0) AS DiffPriceYear2,
                      FORMAT( ((1 - (E.StartPrice2 / E.Price2)) * 100), 2) AS DisCount2,

                      FORMAT( E.Kifu_Month ,0) AS Kifu_Month,
                      FORMAT( E.Power_Year ,0) AS Power_Year,
                      FORMAT( E.FTC_Year   ,0) AS FTC_Year,
                      FORMAT( E.Kifu_Year  ,0) AS Kifu_Year,

                      from
(
SELECT C.UsedPower, C.Price, C.StartPrice, C.UnitPrice, C.ReEnePrice,
C.Price + ( (C.UnitPrice * C.UsedPower) + (C.ReEnePrice * C.UsedPower) ) AS Price2,
C.StartPrice + (C.ReEnePrice * C.UsedPower) AS StartPrice2

from
(SELECT A.UsedPower,
((:Capacity * D.UnitPrice) + A.Price) AS Price,
TRUNCATE ( ((:Capacity * D.StartUnitPrice) + A.StartPrice),0) AS StartPrice,
B.UnitPrice AS UnitPrice, B.ReEnePrice AS ReEnePrice,
A.Kifu_Month, A.Power_Year, A.FTC_Year, A.Kifu_Year
  from " . $table . " AS A
 INNER JOIN power_unitprice AS B ON B.AreaCd = A.AreaCd and B.PriceDate = A.PriceDate
 INNER JOIN power_big_unitprice AS D ON D.AreaCd = A.AreaCd
 where A.AreaCd = :AreaCd
   And A.PriceDate = :PriceDate
   And B.PriceDate = :PriceDate
   And ((:Capacity * D.UnitPrice) + A.Price) <= :PriceQty Order By A.Price Desc Limit 1) AS C)
AS E";
        }
        else {
        $sql = "select E.UsedPower, E.Price,
                      FORMAT(E.Price,0) AS PriceStr,
                      FORMAT( (E.Price * 12),0) AS PriceYear,

                      E.StartPrice,
                      FORMAT(E.StartPrice,0) AS StartPriceStr,
                      FORMAT( (E.StartPrice * 12),0) AS StartPriceYear,

                      FORMAT( (E.Price - E.StartPrice),0) AS DiffPrice,
                      FORMAT( ((E.Price - E.StartPrice) * 12),0) AS DiffPriceYear,
                      FORMAT( ((1 - (E.StartPrice / E.Price)) * 100), 2) AS DisCount,

                      FORMAT( E.Price2,0) AS Price2,
                      FORMAT( E.StartPrice, 0) AS StartPrice2,

                      FORMAT( (E.Price2 * 12),0) AS Price2Year,
                      FORMAT( (E.StartPrice2 * 12), 0) AS StartPrice2Year,

FORMAT( (E.Price2 - E.StartPrice2),0) AS DiffPrice2,
                      FORMAT( ((E.Price2 - E.StartPrice2) * 12),0) AS DiffPriceYear2,
                      FORMAT( ((1 - (E.StartPrice2 / E.Price2)) * 100), 2) AS DisCount2,

                      0 AS Kifu_Year,
                      0 AS Power_Year,
                      0 AS FTC_Year,
                      0 AS Kifu_Year

                      from
(
SELECT C.UsedPower, C.Price, C.StartPrice, C.UnitPrice, C.ReEnePrice,
C.Price + ( (C.UnitPrice * C.UsedPower) + (C.ReEnePrice * C.UsedPower) ) AS Price2,
C.StartPrice + (C.ReEnePrice * C.UsedPower) AS StartPrice2

from
(SELECT A.UsedPower,
((:Capacity * D.UnitPrice) + A.Price) AS Price,
TRUNCATE ( ((:Capacity * D.StartUnitPrice) + A.StartPrice),0) AS StartPrice,
B.UnitPrice AS UnitPrice, B.ReEnePrice AS ReEnePrice
  from " . $table . " AS A
 INNER JOIN power_unitprice AS B ON B.AreaCd = A.AreaCd
 INNER JOIN power_big_unitprice AS D ON D.AreaCd = A.AreaCd
 where A.AreaCd = :AreaCd
   And B.PriceDate = :PriceDate
   And ((:Capacity * D.UnitPrice) + A.Price) <= :PriceQty Order By A.Price Desc Limit 1) AS C)
AS E";
        }

        $sth = $dbh->prepare($sql);

        $sth->bindParam(':AreaCd',    $AreaCd,    PDO::PARAM_INT);
        $sth->bindParam(':PriceDate', $PriceDate, PDO::PARAM_STR);
        $sth->bindParam(':Capacity',  $Capacity,  PDO::PARAM_INT);
        $sth->bindParam(':PriceQty',  $PriceQty,  PDO::PARAM_INT);
    }

    $sth->execute();
    $results = $sth->fetchAll(PDO::FETCH_ASSOC);

    // JSON形式で出力する
    header('Content-Type: application/json');
    echo json_encode( $results );
    exit;
}
catch (PDOException $e)
{
    // 例外処理
    die('Error:' . $e->getMessage());
}

?>