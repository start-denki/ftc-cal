CREATE View v_PowerAmount_Hendo(
AreaCd, AreaName, PriceDate, Capacity,
UsedPower, Price, PriceStr,
AjustPrice, Reused, 
StartPrice, PriceYear, StartPriceYear,
DiffPrice, DiffPriceYear, DisCount,
Price2, StartPrice2,
Price2Year, StartPrice2Year,
DiffPrice2, DiffPriceYear2, DisCount2,
Kifu_Month,
Power_Year,
FTC_Year,
Kifu_Year
)
AS
SELECT C.AreaCd, C.AreaName, C.PriceDate, C.Capacity,
       C.UsedPower,
       C.Price,                                        -- 月々
       FORMAT(C.Price, 0),                             -- 月々
       C.AjustPrice,                      -- 燃料費調整額
       C.Reused,                          -- 再生可能エネルギー促進賦課金

       FORMAT( C.StartPrice, 0),                        -- スタート電気：月々
       FORMAT( (C.Price * 12), 0),                     -- 年間
       FORMAT( (C.StartPrice * 12), 0),                -- スタート電気：年間

       FORMAT( (C.Price - C.StartPrice), 0),           -- 月々の差額
       FORMAT( (C.Power_Year - FTC_Year), 0),          -- 年間差額
       FORMAT(((1 - (C.FTC_Year / C.Power_Year)) * 100), 2),     -- 年間割引率
       
       FORMAT( Price2 ,0), 
       FORMAT( StartPrice2, 0),

       FORMAT( (Price2 * 12),0), 
       FORMAT( (StartPrice2 * 12), 0),

       FORMAT( (C.Price2 - C.StartPrice2), 0),           -- 月々の差額
       FORMAT( ((C.Price2 - C.StartPrice2) * 12), 0),           -- 年間差額
       FORMAT(((1 - (C.StartPrice2 / C.Price2)) * 100), 2),     -- 年間割引率

FORMAT(C.Kifu_Month, 0),
FORMAT(C.Power_Year, 0),
FORMAT(C.FTC_Year, 0),
FORMAT(C.Kifu_Year, 0)

FROM v_PowerAmount_Hendo2 AS C
