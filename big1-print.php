<!DOCTYPE html>
<html>
<head>
    <LINK REL="SHORTCUT ICON" HREF="/favicon.ico">
    <META http-Equiv="Content-Type" Content="text/html; charset=utf-8">
    <META http-Equiv="Content-Style-Type" content="text/css">
    <META http-Equiv="Content-Script-Type" Content="text/javascript">
    <META http-Equiv="Pragma" Content="no-cache">
    <META http-Equiv="Cache-control" Content="no-cache, no-store, must-revalidate, post-check=0, pre-check=0">
    <META http-Equiv="Expires" Content="0">
    <title>スタート電気　料金シミュレーター(プレミアムＢＩＧプラン)印刷</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="stylesheet" type="text/css" href="./formbasic.css" />
    <link rel="stylesheet" type="text/css" href="./style-cal.css" />
</head>
<body>
    <div class="paper_wrapper">
        <div class="header-title-big">
            <h1>スタート電気　料金シミュレータ(プレミアムＢＩＧプラン)</h1>
        </div>

        <p>
            お手元の電気検針票（電気のご使用量のお知らせ）をご確認の上、各項目を入力してください。 
        </p>
        <p>
            直近１か月の「電気使用量」もしくは「電気料金」より、切替メリットをシミュレーション致します。
        </p>

        <h3>現在のご利用状況</h3>
        <div class="caution">
        『＊』が付いている項目は必須入力項目です。
        </div>
        <p>
            下記の試算方法よりどちらか（１か月の電気料金 又はご使用量）をお選び下さい。（※1：最大3,200kWhまで試算可能）
        </p>
        
        <div class="cal-input1big">
            <table>
              <tbody class="input-cal-tbl">
                <tr>
                    <th>電力会社エリア</th>
                    <td>
                        <div id="AreaCd"><?php print ($_GET["p1"]) ?></div>
                    </td>
                </tr>
                <tr>
                    <th>契約種別</th>
                    <td>
                        <div id="PowerType">
                        従量電灯C
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>＊契約容量（kVA）</th>
                    <td>
                        <div id="txtCapacity"><?php print ($_GET["p2"]) ?></div>
                    </td>
                </tr>
              </tbody>
            </table>
            <div>
               ※関西、中国、四国ｴﾘｱは入力不要
            </div>
        </div>

        <div class="cal-input2big">
                <table>
                  <tbody class="input-cal-tbl">
                    <tr>
                        <th>
                            ＊ご使用年月日
                        </th>
                        <td>
                            <div id="PriceDate"><?php print ($_GET["p3"]) ?></div>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            ご使用量（kWh)
                        </th>
                        <td>
                            <div id="txtUsedPower"><?php print (number_format($_GET["p4"])); ?></div>
                        </td>
                        <td>
                            kWh
                        </td>
                    </tr>
                    <tr>
                        <th>
                            １カ月の電気料金（円）
                        </th>
                        <td>
                            <div id="txtPriceQty"><?php print (number_format($_GET["p5"])); ?></div>
                        </td>
                        <td>
                            円（※２）
                        </td>
                    </tr>
                  </tbody>
                </table>
                <div>
                    ※２：基本料金、電力量料金、燃料費調整額、再生可能エネルギー発電促進賦課金を含んだ金額
                </div>
        </div>

        <div style="clear:both;">
        </div>

        <div class="simulation-output">
            <div>
                <div class="cal-output1">
                    <table>
                      <tbody class="output-cal-tbl1">
                        <tr>
                            <th colspan="3">
                                現在の電気料金（概算）
                            </th>
                        </tr>
                        <tr>
                            <th>
                                月々
                            </th>
                            <td>
                                <div id="viewUsedMonth"><?php print (number_format($_GET["p6"])); ?>
                                </div>
                            </td>
                            <td>
                                （※３）
                            </td>
                        </tr>
                        <tr>
                            <th>
                                年間
                            </th>
                            <td>
                                <div id="viewUsedYear"><?php print ($_GET["p7"]) ?>
                                </div>
                            </td>
                            <td></td>
                        </tr>
                      </tbody>
                    </table>
                </div>

                <div class="cal-output2">
                    <table>
                      <tbody class="output-cal-tbl2">
                        <tr>
                            <th colspan="3">
                                スタート電気の電気料金（概算）
                            </th>
                        </tr>
                        <tr>
                            <th>
                                月々
                            </th>
                            <td>
                                <div id="viewPriceMonth"><?php print ($_GET["p8"]) ?>
                                </div>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <th>
                                年間
                            </th>
                            <td>
                                <div id="viewPriceYear"><?php print ($_GET["p9"]) ?>
                                </div>
                            </td>
                            <td></td>
                        </tr>
                      </tbody>
                    </table>
                </div>
            </div>

            <div class="cal-output1-memo" style="clear:both;">
                ※３：将来の再エネ、燃料費調整額は不確定の為除外して算出。ご入力いただいた電気料金の近似値で表示
            </div>
        </div>
            
        <div class="cal-output3">
            <table>
              <tbody class="output-cal-tbl3">
                <tr>
                    <th>
                        月々・・・
                    </th>
                    <td>
                        <div id="viewTotalMonth"><?php print ($_GET["p10"]) ?>
                        </div>
                    </td>
                    <td>
                        おトク！
                    </td>
                    <td>
                        将来の再エネ、燃料費調整額は不確定の為除外して算出
                    </td>
                </tr>
                <tr>
                    <th>
                        年間・・・
                    </th>
                    <td>
                        <div id="viewTotalYear"><?php print ($_GET["p11"]) ?>
                        </div>
                    </td>
                    <td>
                        おトク！
                    </td>
                    <td>
                        将来の再エネ、燃料費調整額は不確定の為除外して算出
                    </td>
                </tr>
                <tr>
                    <th>
                        割引率・・
                    </th>
                    <td>
                        <div id="viewTotalDiscount"><?php print ($_GET["p12"]) ?>
                        </div>
                    </td>
                    <td>
                        おトク！
                    </td>
                    <td>
                        将来の再エネ、燃料費調整額は不確定の為除外して算出
                    </td>
                </tr>
              </tbody>
            </table>
        </div>

        <h3>予想した燃料調整・再エネ発電賦課金を含めた概算請求額の比較</h3>

        <div>
            <table>
             <tbody>
                <tr>
                    <th>
                        予想の燃料調整単価
                    </th>
                    <td id="viewFuelPrice"><?php print ($_GET["p20"]) ?>
                    </td>
                    <td>
                        円
                    </td>
                </tr>
              </tbody>
            </table>
        </div>

        <div>
            <div>
                <div class="cal-output1">
                    <table>
                      <tbody class="output-cal-tbl1">
                        <tr>
                            <th colspan="3">
                                現在の電気料金（概算）
                            </th>
                        </tr>
                        <tr>
                            <th>
                                月々
                            </th>
                            <td>
                                <div id="viewUsedMonth2"><?php print ($_GET["p13"]) ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                年間
                            </th>
                            <td>
                                <div id="viewUsedYear2"><?php print ($_GET["p14"]) ?>
                                </div>
                            </td>
                        </tr>
                      </tbody>
                    </table>
                </div>

                <div class="cal-output2">
                    <table>
                      <tbody class="output-cal-tbl2">
                        <tr>
                            <th colspan="2">
                                スタート電気の電気料金（概算）
                            </th>
                        </tr>
                        <tr>
                            <th>
                                月々
                            </th>
                            <td>
                                <div id="viewPriceMonth2"><?php print ($_GET["p15"]) ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                年間
                            </th>
                            <td>
                                <div id="viewPriceYear2"><?php print ($_GET["p16"]) ?>
                                </div>
                            </td>
                        </tr>
                      </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="cal-output3">
            <table>
              <tbody class="output-cal-tbl3">
                <tr>
                    <th>
                        月々・・・
                    </th>
                    <td>
                        <div id="viewTotalMonth2"><?php print ($_GET["p17"]) ?>
                        </div>
                    </td>
                    <td>
                        おトク！
                    </td>
                </tr>
                <tr>
                    <th>
                        年間・・・
                    </th>
                    <td>
                        <div id="viewTotalYear2"><?php print ($_GET["p18"]) ?>
                        </div>
                    </td>
                    <td>
                        おトク！
                    </td>
                </tr>
                <tr>
                    <th>
                        割引率・・
                    </th>
                    <td>
                        <div id="viewTotalDiscount2"><?php print ($_GET["p19"]) ?>
                        </div>
                    </td>
                    <td>
                        おトク！
                    </td>
                </tr>
              </tbody>
            </table>
        </div>


        <div class="description" style="clear:both;">
            <h2>『スタート電気　料金プランシミュレーション』利用上のご注意 </h2>
            <ul>
             <li>ご入力頂いた値を元に、過去の燃料費調整額、再生可能エネルギー発電促進賦課金データを考慮し、将来の年間電気代を算出しております。</li>
             <li>月額電気代での算出は近似値を利用している為、算出結果が異なります。</li>
             <li>スタート電気の「プレミアムＢＩＧプラン」は別途燃料費調整額相当額を請求いたしません。</li>
             <li>お客様の電気使用量により切替メリット額は異なりますので、上記切替メリット額、割引率を保証するものではありません。</li>
             <li>試算には、深夜機器割引、口座振替割引、各種割引を含んでおりません。 </li>
             <li>本シミュレーションは、ご入力いただいた使用量、条件、およびスタート電気が設定する前提条件に基づいて試算するものです。</li>
             <li>上記「月額電気代」は基本料金、電力量料金、燃料費調整額、再生可能エネルギー発電促進賦課金を含んだ金額をご入力頂きます。</li>
             <li>「年間」は「月々」に12を乗じて算出しております。</li>
             <li>スタート電気の「プレミアムＢＩＧプラン」は別途再生可能エネルギー発電促進賦課金を請求いたします。</li>
             <li>お客様のご使用量が極端に少ない場合や、電力会社の燃料費調整額によっては、ご請求金額が現在よりも高くなる場合がございます。</li>
            </ul>
        </div>
    </div>
</body>
</html>