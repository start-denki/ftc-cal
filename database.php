<?php
/* =====================================================================
Project Name : スタート電気計算処理
ProgramID    : database.php
Copyright    : アスカクリエイト
Notes        :
Modify       :
2017/11/02 越：新規作成
2017/11/07 越：予想の燃料調整単価の追加
2017/12/02 越：予想の燃料調整単価の計算追加
2018/07/21 越：２０１８年７月以降、計算方式が変わるのでテーブルを新規追加 Power_AmountNext
2019/02/02 越：2019年1月以降、計算方式が変わるのでテーブルを新規追加 Power_Amount_hendo
2019/12/09 越：ftc用サイト
2020/05/13 越：PowerAmount_Hendoに４項目を追加
20xx/xx/xx xx:
======================================================================*/
// Ajax通信ではなく、直接URLを叩かれた場合はエラーメッセージを表示
if (
    !(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')
    && (!empty($_SERVER['SCRIPT_FILENAME']) && 'database.php' === basename($_SERVER['SCRIPT_FILENAME']))
    )
{
    die ('このページは直接ロードしないでください。');
}

// 接続文字列 (PHP5.3.6から文字コードが指定できるようになりました)
$dsn = 'mysql:dbname=ftc-energy_cal;host=mysql57.ftc-energy.sakura.ne.jp;charset=utf8';

// ユーザ名
$user = 'ftc-energy';

// パスワード
$password = 'DebLjHZn-';

//ポストで入力値を受け取る
$AreaCd    = $_POST['AreaCd'];         //地区コード
$Capacity  = $_POST['Capacity'];       //容量(A)
$PriceDate = $_POST['PriceDate'];      //年月    YYYYMMDD
$PriceQty  = $_POST['PriceQty'];       //１か月電気料金
$UsedPower = $_POST['UsedPower'];      //ご使用量(kWh)
$CalType   = $_POST['CalType'];        //計算式　０：１か月の電気料金　１：ご使用量（kWh)
$FuelPrice = $_POST['FuelPrice'];      //予想の燃料調整単価

try
{
    // nullで初期化
    $results = null;

    // DBに接続
    $dbh = new PDO($dsn, $user, $password);

    //テーブルのデータを取得する
    $sql = '';
    $table = '';

    if (intval($PriceDate) >= 20190101) {
        $table = 'v_PowerAmount_Hendo';
    }
    else {
        if (intval($AreaCd) == 6 and intval($PriceDate) >= 20180701) {
            $table = 'v_PowerAmountNext';
        }
        else {
            $table = 'v_PowerAmount';
        }
    }

    if ($CalType == '1') {
        $sql = "select UsedPower, Price, PriceStr, StartPrice, PriceYear, StartPriceYear,
                      DiffPrice, DiffPriceYear, DisCount, Price2, StartPrice2,
                      Price2Year, StartPrice2Year,
                      DiffPrice2, DiffPriceYear2, DisCount2,
                      Kifu_Month, Power_Year, FTC_Year, Kifu_Year
                      from " . $table .
                    " where AreaCd = :AreaCd
                       And PriceDate = :PriceDate
                       And Capacity = :Capacity
                       And UsedPower <= :UsedPower Order By UsedPower Desc Limit 1";

        $sth = $dbh->prepare($sql);

        $sth->bindParam(':AreaCd',    $AreaCd,    PDO::PARAM_INT);
        $sth->bindParam(':PriceDate', $PriceDate, PDO::PARAM_STR);
        $sth->bindParam(':Capacity',  $Capacity,  PDO::PARAM_INT);
        $sth->bindParam(':UsedPower', $UsedPower, PDO::PARAM_INT);
    }
    else {
        $sql = "SELECT UsedPower, Price, PriceStr, StartPrice, PriceYear, StartPriceYear,
                      DiffPrice, DiffPriceYear, DisCount, Price2, StartPrice2,
                      Price2Year, StartPrice2Year,
                      DiffPrice2, DiffPriceYear2, DisCount2,
                      Kifu_Month, Power_Year, FTC_Year, Kifu_Year
                      from " . $table .
                   " where AreaCd = :AreaCd
                       And PriceDate = :PriceDate
                       And Capacity = :Capacity
                       And Price <= :PriceQty Order By Price Desc Limit 1";

        $sth = $dbh->prepare($sql);

        $sth->bindParam(':AreaCd',    $AreaCd,    PDO::PARAM_INT);
        $sth->bindParam(':PriceDate', $PriceDate, PDO::PARAM_STR);
        $sth->bindParam(':Capacity',  $Capacity,  PDO::PARAM_INT);
        $sth->bindParam(':PriceQty',  $PriceQty,  PDO::PARAM_INT);
    }

    $sth->execute();
    $results = $sth->fetchAll(PDO::FETCH_ASSOC);

    // JSON形式で出力する
    header('Content-Type: application/json');
    echo json_encode( $results );
    exit;
}
catch (PDOException $e)
{
    // 例外処理
    die('Error:' . $e->getMessage());
}

?>